/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.foundation.util.BundleConfiguration
 */
public interface StatisticsConfiguration {

  public static String DATA_SOURCE_KEY = "datasource";
  public static String DATA_DIR_KEY = "datadir";
  public static String CONNECTION_POOLSIZE_KEY = "connections.poolsize";
  public static String COUNTERS_CACHESIZE_KEY = "cache.counters";
  public static String AGENTCOUNTERS_CACHESIZE_KEY = "cache.agentcounters";
  public static String RRDDB_CACHESIZE_KEY = "cache.rrddb";
  public static String RRDTEMPLATES_CACHESIZE_KEY = "cache.rrdtemplates";

}//interface StatisticsConfiguration
