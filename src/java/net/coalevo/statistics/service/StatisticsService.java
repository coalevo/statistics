/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Service;
import net.coalevo.statistics.model.*;
import org.rrd4j.core.RrdDef;
import org.rrd4j.graph.RrdGraphDef;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Defines the interface of the statistics service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface StatisticsService
    extends Service, Maintainable {

  /**
   * Creates a new {@link Counter}.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the counter.
   * @return the newly created {@link Counter} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public Counter createCounter(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException;

  /**
   * Deletes an existing {@link Counter}.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the counter.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public void deleteCounter(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns an existing {@link Counter}.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the counter.
   * @return a {@link Counter} instance.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   * @throws NoSuchCounterException if no counter with the given identifier exists.
   */
  public Counter getCounter(Agent caller, String identifier)
      throws SecurityException, NoSuchCounterException;

  /**
   * Returns the list of counter identifiers managed by this service.
   *
   * @param caller the requesting {@link Agent}.
   * @return a <tt>List</tt> of identifier strings.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public List<String> listCounters(Agent caller)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns the list of counter identifiers with a given identifier prefix,
   * that are managed by this service.
   *
   * @param caller the requesting {@link Agent}.
   * @param prefix the identifier prefix.
   * @return a <tt>List</tt> of identifier strings.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public List<String> listCounters(Agent caller, String prefix)
      throws SecurityException, StatisticsServiceException;

  /**
   * Creates a new {@link AgentBoundCounter} bound to a specified agent.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the counter.
   * @param aid        the identifier of the agent the new counter should be bound to.
   * @return the newly created {@link AgentBoundCounter} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public Counter createAgentBoundCounter(Agent caller, String identifier, AgentIdentifier aid)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns an existing {@link AgentBoundCounter}.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the counter.
   * @param aid        the identifier of the agent the new counter is bound to.
   * @return an {@link AgentBoundCounter} instance.
   * @throws SecurityException      if the calling {@link Agent} is not authentic or
   *                                not authorized.
   * @throws NoSuchCounterException if no counter with the given identifier bound to the given
   *                                agent exists.
   */
  public Counter getAgentBoundCounter(Agent caller, String identifier, AgentIdentifier aid)
      throws SecurityException, NoSuchCounterException;

  /**
   * Deletes an existing {@link AgentBoundCounter}.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the counter.
   * @param aid        the identifier of the agent the new counter is bound to.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public void deleteAgentBoundCounter(Agent caller, String identifier, AgentIdentifier aid)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns the list of distinct identifiers for {@link AgentBoundCounter} instances
   * managed by this service.
   * <p/>
   *
   * @param caller the requesting {@link Agent}.
   * @return a <tt>List</tt> of identifier strings.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public List<String> listAgentBoundCounters(Agent caller)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns {@link AgentBoundCounterStats} for all {@link AgentBoundCounter}
   * instances managed by this service that have a common identifier.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the counter.
   * @return a <tt>List</tt> of identifier strings.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   * @see #listAgentBoundCounters(Agent)
   */
  public AgentBoundCounterStats getAgentBoundCounterStats(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException;

  /**
   * Creates a new {@link RRD} database with a given definition.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the {@link RRD}.
   * @param definition the RRD database definition.
   * @return the newly created {@link RRD} instance.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   * @see #getRRDDef(Agent,String,Map)
   */
  public RRD createRRD(Agent caller, String identifier, RrdDef definition)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns an existing {@link RRD} database with the given identifier.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the {@link RRD}.
   * @return the {@link RRD} instance.
   * @throws SecurityException  if the calling {@link Agent} is not authentic or
   *                            not authorized.
   * @throws NoSuchRRDException if no RRD database with the given identifier exists.
   */
  public RRD getRRD(Agent caller, String identifier)
      throws SecurityException, NoSuchRRDException;

  /**
   * Deletes an existing {@link RRD} database with the given identifier.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier of the {@link RRD}.
   * @throws SecurityException if the calling {@link Agent} is not authentic or
   *                           not authorized.
   */
  public void deleteRRD(Agent caller, String identifier)
      throws SecurityException;

  /**
   * Renders the graph as defined in memory.
   * <p/>
   * The format of the array is defined by the definition. It is
   * strongly recommended to use a lossless compression format
   * (e.g. PNG) in your definition.
   *
   * @param caller the requesting {@link Agent}.
   * @param gdef   the RRD graph definition.
   * @return the image bytes generated as specified in the definition.
   * @throws IOException if the generation fails.
   * @see #getRRDGraphDef(Agent,String,Map)
   */
  public byte[] renderRRDGraph(Agent caller, RrdGraphDef gdef)
      throws IOException;

  /**
   * Adds an RRD template.
   * <p/>
   * Note that the implementation will set the path for a database definition to
   * a locally stored RRD file (according to its configuration).<br/>
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier for the template.
   * @param template   the template string (should be rrd4j compatible XML).
   * @param isgraph    specifies if this is a graph or a database definition template.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   * @see #getRRDGraphDef(Agent,String,Map)
   * @see #getRRDDef(Agent,String,Map)
   */
  public void addRRDTemplate(Agent caller, String identifier, String template, boolean isgraph)
      throws SecurityException, StatisticsServiceException;

  /**
   * Updates an RRD template.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier for the template.
   * @param template   the template string (should be rrd4j compatible XML).
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public void updateRRDTemplate(Agent caller, String identifier, String template)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns a specified RRD template.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier for the template.
   * @return the template as String.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public String getRRDTemplate(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException;

  /**
   * Removes an RRD template.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier for the template.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws StatisticsServiceException if an error occurs while executing this action.
   */
  public void removeRRDTemplate(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException;

  /**
   * Returns an RRD graph definition for a stored template with the variables applied.
   * <p/>
   * Template variable values for datasources should use the prefix <code>id2f_</code>
   * and then the identifier of the RRD database as managed by this service in
   * the variables value.  These values will be automatically translated to the corresponding
   * filenames.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier for the template.
   * @param vars       the variables to be applied to the template to obtain the specific definition.
   * @return the graph definition.
   * @throws SecurityException       if the calling {@link Agent} is not authentic or
   *                                 not authorized.
   * @throws NoSuchTemplateException if a template with the given identifier does not exist.
   */
  public RrdGraphDef getRRDGraphDef(Agent caller, String identifier, Map vars)
      throws SecurityException, NoSuchTemplateException;

  /**
   * Returns an RRD database definition for a stored template with the variables applied.
   * <p/>
   * Note that the path of the database will be set by the service implementation.
   *
   * @param caller     the requesting {@link Agent}.
   * @param identifier the identifier for the template.
   * @param vars       the variables to be applied to the template to obtain the specific definition.
   * @return the RRD database definition.
   * @throws SecurityException       if the calling {@link Agent} is not authentic or
   *                                 not authorized.
   * @throws NoSuchTemplateException if a template with the given identifier does not exist.
   */
  public RrdDef getRRDDef(Agent caller, String identifier, Map vars)
      throws SecurityException, NoSuchTemplateException;

}//interface StatisticsService
