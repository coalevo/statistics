/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.model;

import net.coalevo.statistics.service.StatisticsService;

/**
 * Exception thrown when the {@link StatisticsService} fails
 * to provide a given service functionality due to an internal
 * error.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class StatisticsServiceException
    extends Exception {

  /**
   * Constructs a new <tt>StatisticsServiceException</tt>.
   */
  public StatisticsServiceException() {
  }//constructor

  /**
   * Constructs a new <tt>StatisticsServiceException</tt>.
   *
   * @param cause the root cause for this exception.
   */
  public StatisticsServiceException(Throwable cause) {
    super(cause);
  }//constructor

  /**
   * Constructs a new <tt>StatisticsServiceException</tt>.
   *
   * @param message the message for this exception.
   * @param cause   the root cause for this exception.
   */
  public StatisticsServiceException(String message, Throwable cause) {
    super(message, cause);
  }//constructor

  /**
   * Constructs a new <tt>StatisticsServiceException</tt>.
   *
   * @param message the message for this exception.
   */
  public StatisticsServiceException(String message) {
    super(message);
  }//constructor

}//class StatisticsServiceException
