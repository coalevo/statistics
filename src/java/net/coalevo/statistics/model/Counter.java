/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.model;

import net.coalevo.foundation.model.Identifiable;

/**
 * Defines a simple counter.
 * <p/>
 * A counter is always greater or equal to zero and
 * can only increase.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Counter
    extends Identifiable {

  /**
   * Increments this counter by one.
   *
   * @return the state of the counter after the increment.
   */
  public long increment();

  /**
   * Increments this counter by a given
   * integer value.
   *
   * @param i the increment to be added to the counter.
   * @return the state of the counter after the increment.
   */
  public long increment(int i);

  /**
   * Increments this counter by a given
   * long value.
   *
   * @param l the increment to be added to the counter.
   * @return the state of the counter after the increment.
   */
  public long increment(long l);

  /**
   * Returns the value of this counter.
   *
   * @return the counter state.
   */
  public long get();

  /**
   * Resets this counter to zero.
   */
  public void reset();

}//interface Counter
