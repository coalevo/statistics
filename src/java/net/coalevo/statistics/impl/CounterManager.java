/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.impl;

import net.coalevo.foundation.util.CacheMapExpelHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.statistics.model.NoSuchCounterException;
import net.coalevo.statistics.model.StatisticsServiceException;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * Provides a manager for counters, handling instance,
 * creation, retrieval and update with a transparent LRU
 * caching mechanism.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class CounterManager
    extends LRUCacheMap<String, CounterImpl> {

  private Marker m_LogMarker = MarkerFactory.getMarker(CounterManager.class.getName());
  private StatisticsStore m_Store;

  public CounterManager(StatisticsStore s, int cachesize) {
    super(cachesize);
    m_Store = s;
    m_ExpelHandler = new ExpelHandler();
  }//CounterManager

  public CounterImpl createCounter(String identifier)
      throws StatisticsServiceException {

    Map<String, String> params = new HashMap<String, String>();
    params.put("counter_id", identifier);
    params.put("counter", Long.toString(0));
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("createCounter", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createCounter()", ex);
      throw new StatisticsServiceException();
    } finally {
      m_Store.releaseConnection(lc);
    }
    return new CounterImpl(identifier, 0);
  }//createCounter

  public void deleteCounter(String identifier)
      throws StatisticsServiceException {

    Map<String, String> params = new HashMap<String, String>();
    params.put("counter_id", identifier);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("deleteCounter", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"deleteCounter()", ex);
      throw new StatisticsServiceException();
    } finally {
      m_Store.releaseConnection(lc);
    }
    //remove from cache
    remove(identifier);
  }//deleteCounter

  public CounterImpl getCounter(String identifier)
      throws NoSuchCounterException {
    CounterImpl c = get(identifier);
    if (c == null) {
      ResultSet rs = null;
      Map<String, String> params = new HashMap<String, String>();
      params.put("counter_id", identifier);
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getCounter", params);
        if (rs.next()) {
          c = new CounterImpl(identifier, rs.getLong(1));
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"getCounter()", ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
      if (c == null) {
        throw new NoSuchCounterException(identifier);
      }
      put(identifier, c);
    }
    return c;
  }//getCounter

  public List<String> listCounters()
      throws StatisticsServiceException {
    //List
    List<String> ids = new ArrayList<String>();
    //Build list
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listCounters", null);
      while (rs.next()) {
        ids.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"listCounters()", ex);
      throw new StatisticsServiceException("listCounters()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return ids;
  }//listCounters


  public List<String> listCounters(String prefix)
      throws StatisticsServiceException {

    //List
    List<String> ids = new ArrayList<String>();
    //Build list
    ResultSet rs = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("counter_id", prefix.concat("%"));
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listCountersWithPrefix", params);
      while (rs.next()) {
        ids.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"listCounters(String)", ex);
      throw new StatisticsServiceException(ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }

    return ids;
  }//listCounters(String)

  public void sync() {
    synchronized (this) {
      for (Iterator iterator = entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        CounterImpl ci = (CounterImpl) entry.getValue();
        updateCounter(ci);
      }
    }
  }//sync

  private void updateCounter(CounterImpl c) {
    if (c.isDirty()) {
      //TODO: commit asynchronous through service?
      Map<String, String> params = new HashMap<String, String>();
      params.put("counter_id", c.getIdentifier());
      params.put("counter", Long.toString(c.get()));
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        lc.executeUpdate("updateCounter", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"updateCounter()", ex);
      } finally {
        m_Store.releaseConnection(lc);
      }
      c.setDirty(false);
    }
  }//updateCounter

  class ExpelHandler implements CacheMapExpelHandler<String, CounterImpl> {

    public void expelled(Map.Entry entry) {
      CounterImpl ci = (CounterImpl) entry.getValue();
      updateCounter(ci);
    }//expelled

  }//inner class ExpelHandler

}//class CounterManager
