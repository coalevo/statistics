/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.impl;

import net.coalevo.foundation.util.CacheMapExpelHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.foundation.util.crypto.DigestUtil;
import net.coalevo.statistics.model.NoSuchRRDException;
import net.coalevo.statistics.model.StatisticsServiceException;
import org.rrd4j.core.RrdDb;
import org.rrd4j.core.RrdDef;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;

/**
 * Provides a manager for RRD databases, handling instance,
 * creation, retrieval and update with a transparent LRU
 * caching mechanism.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class RRDManager extends LRUCacheMap<String, RRDImpl> {

  private Marker m_LogMarker = MarkerFactory.getMarker(RRDManager.class.getName());

  public RRDManager(int ceiling) {
    super(ceiling);
    m_ExpelHandler = new ExpelHandler();
  }//constructor

  public RRDImpl get(String identifier)
      throws NoSuchRRDException {
    RRDImpl rrd = super.get(identifier);
    String filename = getFilename(identifier);
    if (rrd == null || rrd.getDatabase().isClosed()) {
      //load it
      try {
        //1. check if exists
        if (!new File(filename).exists()) {
          //duplicate
          throw new NoSuchRRDException(identifier);
        }
        RrdDb db = new RrdDb(filename);
        //make instance
        rrd = new RRDImpl(identifier, db);
        //cache
        put(identifier, rrd);
      } catch (IOException ex) {
        Activator.log().error(m_LogMarker,"get()", ex);
        throw new NoSuchRRDException(ex.getMessage());
      }

    }
    return rrd;
  }//get

  public RRDImpl create(String identifier, RrdDef def)
      throws StatisticsServiceException {

    String filename = getFilename(identifier);
    try {
      //1. check if exists
      if (new File(filename).exists()) {
        throw new StatisticsServiceException("duplicate");
      }
      //2. Set path
      def.setPath(filename);
      RrdDb db = new RrdDb(def);
      RRDImpl rrd = new RRDImpl(identifier, db);
      put(identifier, rrd);
      return rrd;
    } catch (IOException ex) {
      Activator.log().error(m_LogMarker,"create()", ex);
      throw new StatisticsServiceException(ex);
    }
  }//create

  public void destroy(String identifier) {
    String filename = getFilename(identifier);
    //1. check if exists
    File f = new File(filename);
    f.delete();
    remove(identifier);
  }//destroy

  public void sync() {
    synchronized (this) {
      for (Iterator iterator = entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        RRDImpl rrd = (RRDImpl) entry.getValue();
        try {
          rrd.getDatabase().close();
        } catch (IOException e) {
          Activator.log().error(m_LogMarker,"sync()", e);
        }
      }
    }
  }//sync

  /**
   * Returns a filename for this database that is save to be used
   * on any platform (fixed length hex string), and within the services
   * store.
   *
   * @param identifier the identifier.
   * @return the filename for this database.
   */
  public String getFilename(String identifier) {
    return new StringBuilder().append(Activator.toStoreFile(getEncodedIdentifier(identifier)))
        .append(".rrd").toString();
  }//getFilename

  /**
   * Generates an encoding of the identifier that wont be a problem for
   * any file system.
   *
   * @param identifier the unencoded identifier.
   * @return the encoded identifier.
   */
  protected String getEncodedIdentifier(String identifier) {
    byte[] id = null;

    try {
      id = DigestUtil.digest(identifier.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      //UTF-8 is default in Java
      return "failed_encoding";
    }
    //double size, two bytes (hex range) for one byte
    StringBuilder buf = new StringBuilder(id.length * 2);
    for (int i = 0; i < id.length; i++) {
      //don't forget the second hex digit
      if (((int) id[i] & 0xff) < 0x10) {
        buf.append("0");
      }
      buf.append(Long.toString((int) id[i] & 0xff, 16));
    }
    return buf.toString();
  }//encodeIdentifier

  class ExpelHandler implements CacheMapExpelHandler<String, RRDImpl> {

    public void expelled(Map.Entry entry) {
      RRDImpl rrd = (RRDImpl) entry.getValue();
      try {
        rrd.getDatabase().close();
      } catch (IOException e) {
        Activator.log().error(m_LogMarker,"expelled()", e);
      }
    }//expelled

  }//inner class ExpelHandler

}//class RRDManager
