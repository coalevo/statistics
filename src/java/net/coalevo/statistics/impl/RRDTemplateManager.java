/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.impl;

import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.statistics.model.NoSuchTemplateException;
import net.coalevo.statistics.model.StatisticsServiceException;
import org.rrd4j.core.RrdDefTemplate;
import org.rrd4j.core.XmlTemplate;
import org.rrd4j.graph.RrdGraphDefTemplate;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a manager for RRD graph templates handling instance,
 * creation, retrieval and update with a transparent LRU
 * caching mechanism.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class RRDTemplateManager extends LRUCacheMap<String, XmlTemplate> {

  private Marker m_LogMarker = MarkerFactory.getMarker(RRDTemplateManager.class.getName());
  private StatisticsStore m_Store;

  public RRDTemplateManager(StatisticsStore store, int ceiling) {
    super(ceiling);
    m_Store = store;
  }//RRDGraphDef

  public void addTemplate(String identifier, String template, boolean isgraph)
      throws StatisticsServiceException {
    try {
      if (isgraph) {
        RrdGraphDefTemplate t = new RrdGraphDefTemplate(template);
        put(identifier, t);
      } else {
        RrdDefTemplate t = new RrdDefTemplate(template);
        put(identifier, t);
      }
    } catch (IOException ex) {
      Activator.log().error(m_LogMarker,"addTemplate", ex);
      throw new StatisticsServiceException(ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("template_id", identifier);
    params.put("template", template);
    params.put("graph", (isgraph) ? "1" : "0");
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("addGDTemplate", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"addTemplate()", ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//addTemplate

  public XmlTemplate getTemplate(String identifier) {
    XmlTemplate t = this.get(identifier);
    if (t == null) {
      //1. load template from database
      ResultSet rs = null;
      Map<String, String> params = new HashMap<String, String>();
      params.put("template_id", identifier);
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getTemplate", params);
        if (rs.next()) {
          if (rs.getShort(2) == 1) {
            t = new RrdGraphDefTemplate(rs.getString(1));
          } else {
            t = new RrdDefTemplate(rs.getString(1));
          }
        } else {
          throw new NoSuchTemplateException();
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,Activator.getBundleMessages().get("RRDTemplateManager.getfailed"), ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
    }
    return t;
  }//getTemplate

  public String getRawTemplate(String identifier) {
    //1. load template from database
    ResultSet rs = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("template_id", identifier);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getTemplate", params);
      if (rs.next()) {
        return rs.getString(1);
      } else {
        throw new NoSuchTemplateException();
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,Activator.getBundleMessages().get("RRDTemplateManager.getfailed"), ex);
      return "";
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//getRawTemplate

  public void updateTemplate(String identifier, String template) {
    Map<String, String> params = new HashMap<String, String>();
    params.put("template_id", identifier);
    params.put("template", template);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("updateTemplate", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"updateTemplate()", ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    //remove from cache
    remove(identifier);
  }//updateTemplate

  public void removeTemplate(String identifier) {
    Map<String, String> params = new HashMap<String, String>();
    params.put("template_id", identifier);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("removeTemplate", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"removeTemplate()", ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    //remove from cache
    remove(identifier);
  }//removeTemplate

}//class RRDTemplateManager
