/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.statistics.model.AgentBoundCounter;

/**
 * Provides an implementation of {@link AgentBoundCounter}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AgentBoundCounterImpl
    extends CounterImpl
    implements AgentBoundCounter {

  private AgentIdentifier m_AgentIdentifier;

  public AgentBoundCounterImpl(String id, AgentIdentifier aid) {
    super(id);
    m_AgentIdentifier = aid;
  }//AgentBoundCounterImpl

  public AgentBoundCounterImpl(String id, long count, AgentIdentifier aid) {
    super(id, count);
    m_AgentIdentifier = aid;
  }//AgentBoundCounterImpl

  public AgentIdentifier getAgentIdentifier() {
    return m_AgentIdentifier;
  }//getAgentIdentifier

}//class AgentBoundCounterImpl
