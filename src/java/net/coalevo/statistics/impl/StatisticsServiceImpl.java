/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.statistics.model.*;
import net.coalevo.statistics.service.StatisticsService;
import org.osgi.framework.BundleContext;
import org.rrd4j.core.RrdDef;
import org.rrd4j.core.RrdDefTemplate;
import org.rrd4j.graph.RrdGraph;
import org.rrd4j.graph.RrdGraphDef;
import org.rrd4j.graph.RrdGraphDefTemplate;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Provides an implementation of {@link StatisticsService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class StatisticsServiceImpl
    extends BaseService
    implements StatisticsService {

  private Marker m_LogMarker = MarkerFactory.getMarker(StatisticsServiceImpl.class.getName());

  private Messages m_BundleMessages;
  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private StatisticsStore m_Store;

  private CounterManager m_CounterManager;
  private AgentBoundCounterManager m_AgentBoundCounterManager;
  private RRDManager m_RRDManager;
  private RRDTemplateManager m_RRDTemplateManager;

  public StatisticsServiceImpl(StatisticsStore store) {
    super(StatisticsService.class.getName(), ACTIONS);
    m_Store = store;
  }//StatisticsServiceImpl


  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/StatisticsService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //4. prepare refs
    m_CounterManager = m_Store.getCounterManager();
    m_AgentBoundCounterManager = m_Store.getAgentBoundCounterManager();
    m_RRDManager = m_Store.getRRDManager();
    m_RRDTemplateManager = m_Store.getRRDTemplateManager();

    return true;
  }//activate

  public boolean deactivate() {

    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }
    //Managers
    if (m_CounterManager != null) {
      m_CounterManager.clear(true);
      m_CounterManager = null;
    }
    if (m_AgentBoundCounterManager != null) {
      m_AgentBoundCounterManager.clear(true);
      m_AgentBoundCounterManager = null;
    }

    m_RRDManager = null;
    m_RRDTemplateManager = null;


    m_Store = null;
    return true;
  }//deactivate


  public void doMaintenance(Agent caller)
      throws SecurityException, MaintenanceException {
  }//doMaintainance

  //*** Counter Handling ***//

  public Counter createCounter(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_COUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_COUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_CounterManager.createCounter(identifier);
  }//createCounter

  public void deleteCounter(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DELETE_COUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DELETE_COUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    m_CounterManager.deleteCounter(identifier);
  }//deleteCounter

  public Counter getCounter(Agent caller, String identifier)
      throws SecurityException, NoSuchCounterException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_COUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_COUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_CounterManager.getCounter(identifier);
  }//getCounter

  public List<String> listCounters(Agent caller)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_COUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_COUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_CounterManager.listCounters();
  }//listCounters

  public List<String> listCounters(Agent caller, String prefix)
      throws SecurityException, StatisticsServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_COUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_COUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }
    return m_CounterManager.listCounters(prefix);
  }//listCounters

  //*** END Counter Handling ***//

  //*** Agent Bound Counter Handling ***//

  public Counter createAgentBoundCounter(Agent caller, String identifier, AgentIdentifier aid)
      throws SecurityException, StatisticsServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_AGENTCOUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_AGENTCOUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_AgentBoundCounterManager.createCounter(identifier, aid);
  }//createAgentBoundCounter

  public Counter getAgentBoundCounter(Agent caller, String identifier, AgentIdentifier aid)
      throws SecurityException, NoSuchCounterException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_AGENTCOUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_AGENTCOUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_AgentBoundCounterManager.getCounter(identifier, aid);
  }//getAgentBoundCounter

  public void deleteAgentBoundCounter(Agent caller, String identifier, AgentIdentifier aid)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DELETE_AGENTCOUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DELETE_AGENTCOUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    m_AgentBoundCounterManager.deleteCounter(identifier, aid);
  }//removeAgentBoundCounter

  public List<String> listAgentBoundCounters(Agent caller)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_AGENTCOUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_AGENTCOUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_AgentBoundCounterManager.listCounters();
  }//listAgentBoundCounters

  public AgentBoundCounterStats getAgentBoundCounterStats(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_AGENTCOUNTER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_AGENTCOUNTER.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_AgentBoundCounterManager.getCounterStats(identifier);
  }//listCounters

  //*** END Agent Bound Counter Handling ***//

  //*** RRD Database Handling ***//

  public RRD createRRD(Agent caller, String identifier, RrdDef def)
      throws SecurityException, StatisticsServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CREATE_RRD);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CREATE_RRD.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_RRDManager.create(identifier, def);
  }//createRRD

  public RRD getRRD(Agent caller, String identifier)
      throws SecurityException, NoSuchRRDException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_RRD);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_RRD.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_RRDManager.get(identifier);
  }//getRRD

  public void deleteRRD(Agent caller, String identifier)
      throws SecurityException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DELETE_RRD);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DELETE_RRD.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    m_RRDManager.destroy(identifier);
  }//deleteRRD

  //*** END RRD Database Handling ***//

  public byte[] renderRRDGraph(Agent caller, RrdGraphDef gdef)
      throws IOException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), RENDER_RRDGRAPH);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", RENDER_RRDGRAPH.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //create in memory
    gdef.setFilename("-");
    RrdGraph g = new RrdGraph(gdef);
    return g.getRrdGraphInfo().getBytes();
  }//renderRRDGraph

  //*** RRD Templates Handling ***//

  public void addRRDTemplate(Agent caller, String identifier, String template, boolean isgraph)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), ADD_RRDTEMPLATE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", ADD_RRDTEMPLATE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    m_RRDTemplateManager.addTemplate(identifier, template, isgraph);
  }//addRRDTemplate

  public void updateRRDTemplate(Agent caller, String identifier, String template)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), UPDATE_RRDTEMPLATE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", UPDATE_RRDTEMPLATE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    m_RRDTemplateManager.updateTemplate(identifier, template);
  }//updateRRDTemplate

  public String getRRDTemplate(Agent caller, String identifier)
      throws SecurityException, NoSuchTemplateException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_RRDTEMPLATE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_RRDTEMPLATE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    return m_RRDTemplateManager.getRawTemplate(identifier);
  }//getRRDTemplate

  public void removeRRDTemplate(Agent caller, String identifier)
      throws SecurityException, StatisticsServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_RRDTEMPLATE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", REMOVE_RRDTEMPLATE.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    m_RRDTemplateManager.removeTemplate(identifier);
  }//removeRRDTemplate

  public RrdGraphDef getRRDGraphDef(Agent caller, String tid, Map vars)
      throws SecurityException, NoSuchTemplateException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_RRDGRAPHDEF);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_RRDGRAPHDEF.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //1. Retrieve template
    RrdGraphDefTemplate t =
        (RrdGraphDefTemplate) m_RRDTemplateManager.getTemplate(tid);

    synchronized (t) {
      t.clearValues();
      //2. Handle variables
      for (Iterator iterator = vars.entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        String v = entry.getValue().toString();
        if (v.startsWith("id2f_")) {
          v = m_RRDManager.getFilename(v.substring(5));
        }
        t.setVariable(entry.getKey().toString(), v);
      }
      //3. Create definition
      return t.getRrdGraphDef();
    }
  }//getRRDGraphDef

  public RrdDef getRRDDef(Agent caller, String tid, Map vars)
      throws SecurityException, NoSuchTemplateException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_RRDDEF);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_RRDDEF.getIdentifier(), "caller", caller.getIdentifier()), ex);
    }

    //1. Retrieve template
    RrdDefTemplate t =
        (RrdDefTemplate) m_RRDTemplateManager.getTemplate(tid);

    synchronized (t) {
      t.clearValues();
      //2. Handle variables
      for (Iterator iterator = vars.entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        t.setVariable(entry.getKey().toString(), entry.getValue().toString());
      }
      //3. Create definition
      return t.getRrdDef();
    }
  }//getRRDDef

  //*** END RRD Templates Handling ***//

  private static Action CREATE_COUNTER = new Action("createCounter");
  private static Action DELETE_COUNTER = new Action("deleteCounter");
  private static Action GET_COUNTER = new Action("getCounter");

  private static Action CREATE_AGENTCOUNTER = new Action("createAgentCounter");
  private static Action DELETE_AGENTCOUNTER = new Action("deleteAgentCounter");
  private static Action GET_AGENTCOUNTER = new Action("getAgentCounter");

  private static Action CREATE_RRD = new Action("createRRD");
  private static Action GET_RRD = new Action("getRRD");
  private static Action DELETE_RRD = new Action("deleteRRD");

  private static Action RENDER_RRDGRAPH = new Action("renderRRDGraph");

  private static Action ADD_RRDTEMPLATE = new Action("addRRDTemplate");
  private static Action GET_RRDTEMPLATE = new Action("getRRDTemplate");
  private static Action UPDATE_RRDTEMPLATE = new Action("updateRRDTemplate");
  private static Action REMOVE_RRDTEMPLATE = new Action("removeRRDTemplate");

  private static Action GET_RRDDEF = new Action("getRRDDef");
  private static Action GET_RRDGRAPHDEF = new Action("getRRDGraphDef");


  private static Action[] ACTIONS = {
      CREATE_COUNTER, DELETE_COUNTER, GET_COUNTER,
      CREATE_AGENTCOUNTER, DELETE_AGENTCOUNTER, GET_AGENTCOUNTER,
      CREATE_RRD, GET_RRD, DELETE_RRD, RENDER_RRDGRAPH,
      ADD_RRDTEMPLATE, UPDATE_RRDTEMPLATE, REMOVE_RRDTEMPLATE,
      GET_RRDDEF, GET_RRDGRAPHDEF, GET_RRDTEMPLATE,
      Maintainable.DO_MAINTENANCE, Restoreable.DO_BACKUP, Restoreable.DO_RESTORE
  };

}//class StatisticsServiceImpl
