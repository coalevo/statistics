/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.impl;

import net.coalevo.foundation.model.BackupException;
import net.coalevo.foundation.model.MaintenanceException;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.model.RestoreException;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.statistics.service.StatisticsConfiguration;
import net.coalevo.datasource.service.DataSourceService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.NoSuchElementException;


/**
 * Implements an RDBMS based store for discussion data.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class StatisticsStore
    implements ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(StatisticsStore.class.getName());
  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;

  private Messages m_BundleMessages;

  //Concurrency handling for backup and restore
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_LeaseLatch;
  private CountDownLatch m_RestoreLatch;

  //Managers
  private CounterManager m_CounterManager;
  private AgentBoundCounterManager m_AgentBoundCounterManager;
  private RRDManager m_RRDManager;
  private RRDTemplateManager m_RRDTemplateManager;

  public StatisticsStore() {

  }//constructor

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(m_LogMarker,"releaseConnection()", e);
    }
  }//releaseConnection

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        createSchema(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private synchronized boolean createSchema(LibraryConnection lc) {
    try {
      lc = leaseConnection();
      //1. Create schema, tables and index
      lc.executeCreate("createSchema");
      lc.executeCreate("createCounterTable");
      lc.executeCreate("createAgentCounterTable");
      lc.executeCreate("createRRDTemplateTable");
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createSchema()", ex);
    } finally {
      releaseConnection(lc);
    }
    return false;
  }//createSchema

  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();
    //1. Configure from persistent configuration
    String ds = "embedded";
    int cpoolsize = 2;
    int countercachesize = 25;
    int abccachesize = 50;
    int rrddbcachesize = 25;
    int rrdtcachesize = 25;

    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();

    //Retrieve config
    try {
      ds = mtd.getString(StatisticsConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(StatisticsConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      countercachesize = mtd.getInteger(StatisticsConfiguration.COUNTERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.COUNTERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      abccachesize = mtd.getInteger(StatisticsConfiguration.AGENTCOUNTERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.AGENTCOUNTERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      rrddbcachesize = mtd.getInteger(StatisticsConfiguration.RRDDB_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.RRDDB_CACHESIZE_KEY),
          ex
      );
    }
    try {
      rrdtcachesize = mtd.getInteger(StatisticsConfiguration.RRDTEMPLATES_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.RRDTEMPLATES_CACHESIZE_KEY),
          ex
      );
    }
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(StatisticsStore.class, "net/coalevo/statistics/impl/statisticsstore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"activate()", ex);
      return false;
    }
    DataSourceService dss = Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.datasource"),
          nse
      );
      return false;
    }
    //prepare store
    prepareDataSource();
    Activator.log().info(m_LogMarker,
        m_BundleMessages.get("StatisticsStore.database.info", "source", m_DataSource.toString())
    );

    //Create Managers
    m_CounterManager = new CounterManager(this, countercachesize);
    m_AgentBoundCounterManager = new AgentBoundCounterManager(this, abccachesize);
    m_RRDManager = new RRDManager(rrddbcachesize);
    m_RRDTemplateManager = new RRDTemplateManager(this, rrdtcachesize);

    //add handler config updates
    cm.addUpdateHandler(this);
    return true;
  }//activate

  public synchronized boolean deactivate() {
    if (m_CounterManager != null) {
      //will sync
      m_CounterManager.clear(true);
      m_CounterManager = null;
    }
    if (m_AgentBoundCounterManager != null) {
      //will sync
      m_AgentBoundCounterManager.clear(true);
      m_AgentBoundCounterManager = null;
    }
    if (m_RRDManager != null) {
      //will sync
      m_RRDManager.clear(true);
      m_RRDManager = null;
    }

    //1. close all leased connections
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
        m_ConnectionPool = null;
      } catch (Exception e) {
        Activator.log().error(m_LogMarker,"deactivate()", e);
      }
    }
    m_DataSource = null;
    m_SQLLibrary = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {
    //1. Configure from persistent configuration
    int cpoolsize = 2;
    int countercachesize = 25;
    int abccachesize = 50;
    int rrddbcachesize = 25;
    int rrdtcachesize = 25;

    //Retrieve config
    try {
      cpoolsize = mtd.getInteger(StatisticsConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      countercachesize = mtd.getInteger(StatisticsConfiguration.COUNTERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.COUNTERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      abccachesize = mtd.getInteger(StatisticsConfiguration.AGENTCOUNTERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.AGENTCOUNTERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      rrdtcachesize = mtd.getInteger(StatisticsConfiguration.RRDTEMPLATES_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.RRDTEMPLATES_CACHESIZE_KEY),
          ex
      );
    }
    try {
      rrddbcachesize = mtd.getInteger(StatisticsConfiguration.RRDDB_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("StatisticsStore.activation.configexception", "attribute", StatisticsConfiguration.RRDDB_CACHESIZE_KEY),
          ex
      );
    }

    //Update settings
    m_ConnectionPool.setMaxActive(cpoolsize);
    m_ConnectionPool.setMaxIdle(cpoolsize);
    m_CounterManager.setCeiling(countercachesize);
    m_AgentBoundCounterManager.setCeiling(abccachesize);
    m_RRDManager.setCeiling(rrddbcachesize);
    m_RRDTemplateManager.setCeiling(rrdtcachesize);
  }//update

  //*** Caches and Managers ***//

  public CounterManager getCounterManager() {
    return m_CounterManager;
  }//getCounterManager

  public AgentBoundCounterManager getAgentBoundCounterManager() {
    return m_AgentBoundCounterManager;
  }//getAgentBoundCounterManager

  public RRDManager getRRDManager() {
    return m_RRDManager;
  }//getRRDManager

  public RRDTemplateManager getRRDTemplateManager() {
    return m_RRDTemplateManager;
  }//getRRDTemplateManager

  /**
   * Backups the database of userdata while running.
   * The database will be frozen for writes, but no interruption
   * for reads is required.
   *
   * @param f   the directory to write the backup to.
   * @param tag a possible tag for the backup.
   * @throws SecurityException
   * @throws BackupException   g
   */
  public void backup(File f, String tag)
      throws BackupException {
    /*
    //prepare file
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(f, tag);
    } else {
      backup = f;
    }
    //Get connection
    Connection conn = null;
    try {
      conn = m_DataSource.getConnection();
    } catch (SQLException e) {
      Activator.log().error("doBackup()", e);
      throw new BackupException("UserdataStore:: Could not obtain db connection.", e);
    }

    //1. Backup Database
    try {
      CallableStatement cs =
          conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
      cs.setString(1, backup.getAbsolutePath());
      cs.execute();
      cs.close();
      Activator.log().info(
          m_BundleMessages.get("StatisticsStore.backup.done", "path",
              backup.getAbsolutePath())
      );
    } catch (Exception ex) {
      Activator.log().error("doBackup()", ex);
    }
    */
  }//doBackup

  public void restore(File f, String tag)
      throws RestoreException {
    /*
    //1. Latch for future leases
    m_RestoreLatch = new CountDownLatch(1);
    m_LeaseLatch = new CountDownLatch(1);

    try {
      m_RestoreLatch.await();
    } catch (InterruptedException ex) {
      Activator.log().error("restore()", ex);
    }

    //prepare file
    File bdir = new File(f, StatisticsStore.class.getName());
    if (!bdir.exists()) {
      throw new RestoreException("StatisticsStore:: Failed obtain directory of backup.");
    }
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(bdir, tag);
    } else {
      backup = bdir;
    }
    //Restore from connection
    //Connection conn = null;
    m_DataSource.setConnectionAttributes(";restoreFrom=" + backup.getAbsolutePath());

    try {
      maintain();
    } catch (MaintenanceException ex) {
      Activator.log().error("restore()", ex);
    } finally {
      m_DataSource.setConnectionAttributes("");
    }

    m_RestoreLatch = null;
    m_LeaseLatch.countDown();
    m_LeaseLatch = null;
    Activator.log().info(m_BundleMessages.get("StatisticsStore.restore.done"));
    */
  }//restore

  public void maintain() throws MaintenanceException {
    /*
    String[] tables = {
        "counters",
        "agentcounters",
        "rrdtemplates"
    };
    //Sync caches?

    //Get connection
    Connection conn = null;
    try {
      Activator.log().debug("maintain()::connection = jdbc:derby:" +
          m_DataSource.getDatabaseName() +
          m_DataSource.getConnectionAttributes());
      conn = m_DataSource.getConnection();
    } catch (SQLException e) {
      Activator.log().error("maintain()", e);
      throw new MaintenanceException("StatisticsStore::Could not obtain db connection.", e);
    }

    //1. checkpoint database
    try {
      Activator.log().debug("maintain()::Executing Database Checkpoint");
      CallableStatement cs = conn.prepareCall("CALL SYSCS_UTIL.SYSCS_CHECKPOINT_DATABASE()");
      cs.execute();
      cs.close();
      Activator.log().info(m_BundleMessages.get("StatisticsStore.maintenance.checkpoint"));
    } catch (Exception ex) {
      Activator.log().error("maintain()", ex);
    }

    //2. Compress database tables
    try {

      Activator.log().debug("maintain():: Compressing Database Tables");
      //NOTE: Calls are not case insensitive, 10.2.1.6 needs uppercase
      CallableStatement cs = conn.prepareCall("CALL SYSCS_UTIL.SYSCS_COMPRESS_TABLE(?, ?, ?)");
      cs.setString(1, "cstatistics".toUpperCase()); //schema
      cs.setShort(3, (short) 0); //non sequential, means less time, more space

      for (int i = 0; i < tables.length; i++) {
        String table = tables[i];
        cs.setString(2, table.toUpperCase());
        cs.execute();
        Activator.log().info(
            m_BundleMessages.get("StatisticsStore.maintenance.table", "table", table)
        );
      }
      Activator.log().info(
          m_BundleMessages.get("StatisticsStore.maintenance.done"));

    } catch (Exception ex) {
      Activator.log().error("maintain()", ex);
    }
    */
  }//maintain

  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate",null);
      } catch (SQLException ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

}//class StatisticsStore
