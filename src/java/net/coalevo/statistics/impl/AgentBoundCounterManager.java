/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.statistics.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.CacheMapExpelHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.statistics.model.AgentBoundCounterStats;
import net.coalevo.statistics.model.NoSuchCounterException;
import net.coalevo.statistics.model.StatisticsServiceException;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.sql.ResultSet;
import java.util.*;

/**
 * Provides a manager for counters, handling instance,
 * creation, retrieval and update with a transparent LRU
 * caching mechanism.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AgentBoundCounterManager
    extends LRUCacheMap<String, AgentBoundCounterImpl> {

  private Marker m_LogMarker = MarkerFactory.getMarker(AgentBoundCounterManager.class.getName());
  private StatisticsStore m_Store;

  public AgentBoundCounterManager(StatisticsStore s, int cachesize) {
    super(cachesize);
    m_Store = s;
    m_ExpelHandler = new ExpelHandler();
  }//CounterManager

  public AgentBoundCounterImpl createCounter(String identifier, AgentIdentifier aid)
      throws StatisticsServiceException {
    Map<String, String> params = new HashMap<String, String>();
    params.put("counter_id", identifier);
    params.put("agent_id", aid.getIdentifier());
    params.put("counter", Long.toString(0));
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("createAgentCounter", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createAgentCounter()", ex);
      throw new StatisticsServiceException(ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    //create instance
    AgentBoundCounterImpl abci = new AgentBoundCounterImpl(identifier, 0, aid);
    //add to cache
    put(toCacheId(identifier, aid), abci);
    return abci;
  }//createCounter

  public void deleteCounter(String identifier, AgentIdentifier aid)
      throws StatisticsServiceException {

    Map<String, String> params = new HashMap<String, String>();
    params.put("counter_id", identifier);
    params.put("agent_id", aid.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("deleteAgentCounter", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"deleteAgentCounter()", ex);
      throw new StatisticsServiceException();
    } finally {
      m_Store.releaseConnection(lc);
    }
    //remove from cache
    remove(toCacheId(identifier, aid));
  }//deleteCounter

  public AgentBoundCounterImpl getCounter(String identifier, AgentIdentifier aid)
      throws NoSuchCounterException {
    final String cacheid = toCacheId(identifier, aid);
    AgentBoundCounterImpl c = get(cacheid);
    if (c == null) {
      ResultSet rs = null;
      Map<String, String> params = new HashMap<String, String>();
      params.put("counter_id", identifier);
      params.put("agent_id", aid.getIdentifier());

      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getAgentCounter", params);
        if (rs.next()) {
          c = new AgentBoundCounterImpl(identifier, rs.getLong(1), aid);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"getCounter()", ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
      if (c == null) {
        throw new NoSuchCounterException(cacheid);
      }
      put(cacheid, c);
    }
    return c;
  }//getCounter

  public List<String> listCounters() throws StatisticsServiceException {

    //List
    List<String> ids = new ArrayList<String>();
    //Build list
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listAgentCounters", null);
      while (rs.next()) {
        ids.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"listCounters()::", ex);
      throw new StatisticsServiceException("listCounters()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return ids;
  }//listCounters

  public AgentBoundCounterStats getCounterStats(String cid)
      throws StatisticsServiceException {

    AgentBoundCounterStats abcs = null;
    ResultSet rs = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("counter_id", cid);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getAgentCounterStats", params);
      if (rs.next()) {
        abcs = new AgentBoundCounterStatsImpl(
            rs.getLong(1), //min
            rs.getLong(2), //max
            rs.getLong(3), //sum
            rs.getDouble(4) //average
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"getCounterStats(String)::", ex);
      //TODO: Add message
      throw new StatisticsServiceException();
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }

    return abcs;
  }//getCounterStats(String)

  public void sync() {
    synchronized (this) {
      for (Iterator iterator = entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        AgentBoundCounterImpl ci = (AgentBoundCounterImpl) entry.getValue();
        updateCounter(ci);
      }
    }
  }//sync

  private void updateCounter(AgentBoundCounterImpl c) {
    if (c.isDirty()) {
      //TODO: commit asynchronous through service?
      Map<String, String> params = new HashMap<String, String>();
      params.put("counter_id", c.getIdentifier());
      params.put("agent_id", c.getAgentIdentifier().getIdentifier());
      params.put("counter", Long.toString(c.get()));
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        lc.executeUpdate("updateAgentCounter", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"updateCounter()", ex);
      } finally {
        m_Store.releaseConnection(lc);
      }
      c.setDirty(false);
    }
  }//updateCounter

  private String toCacheId(String id, AgentIdentifier aid) {
    return new StringBuilder().append(id).append(aid.getIdentifier()).toString();
  }//toCacheId

  class ExpelHandler implements CacheMapExpelHandler<String, AgentBoundCounterImpl> {

    public void expelled(Map.Entry entry) {
      AgentBoundCounterImpl ci = (AgentBoundCounterImpl) entry.getValue();
      updateCounter(ci);
    }//expelled

  }//inner class ExpelHandler

}//class AgentBoundCounterManager
